# Home work: Learn React

### Purpose of exercise
Prove that you are willing and able to learn a new technology in a short amount of time.


### Tasks
 - Familiarize yourself with React iself in whichever way you prefer (Feel free to check suggested links at the bottom of this page)
 - Build a small single-page application using React
 - The application should fulfill the following user stories
   1. As a participant I wish to a see a list of all questions given to me by other participants
   - As a participant I wish to be able to answer a specific question
   - As a participant I wish to be able to see previous answers I have given to questions
 - Push your implementation to a git repository (at [Github](https://github.com/) or [GitLab](https://gitlab.com/)) and send a link or invite [niels@qvest.io](mailto:niels@qvest.io) if it is private

### Concept glossary
 - **QVEST**: Flipped survey
 - **Participant:** Someone attending a QVEST
 - **Question:** A string sent to a participant from another participant anonymously
 - **Answer:** A string sent in response to a question

### Scope
 - Aesthetics is not a criteria but do ensure the page is readable and runs on newer browsers
 - You may assume that a colleague will wire the app up to a backend API. If this was a real task at work you would probably be expected to write the necessary code to invoke a backend yourself.
 - Even though the user stories mention multiple participants you are not expected to implement a multi-user system at all. An answer string is considered sent when it has been stored in the client-side state.

### Example data
Feel free to use this data as initial content in your solution
```json
[
  {
    "questionId": "67d020fe-f299-430f-bc84-a8067e0d96da",
    "createdAt": "2010-01-01T13:00:00.000Z",
    "questionContent": "What was the first SaaS product that you ever used?",
    "questionSentAt": "2010-01-06T13:00:00.000Z"
  },
  {
    "questionId": "ca6fb1fe-cbdd-48dc-b98d-95d78c0618ca",
    "createdAt": "2010-01-01T13:00:00.000Z",
    "questionContent": "How do online tools differ from other tools developed by human beings throughout history? What are the unique pitfalls and opportunities?",
    "questionSentAt": "2010-01-03T13:00:00.000Z",
    "answerContent": "Interesting question. I think that you see your customers face-to-face to the lowest extend ever and there is so much noise on social media that it can become misleading or useless. Due to that I believe it is easier than ever to forget to listen to the customers and misunderstand their real pains/gains. I'm a technical guy, and the biggest win for cloud-based systems from my perspective is the observability and access. It's simply much easier to extend, improve and fix things in such a system compared to traditional on-site licensed products.",
    "answerSentAt": "2010-01-03T13:00:00.000Z"
  }
]
```

### Suggested links
 - [Thinking in React](https://reactjs.org/docs/thinking-in-react.html) (Outline of defining principles)
 - [Try React](https://reactjs.org/docs/try-react.html) (Instant online development setup)
 - [Create-react-app](https://github.com/facebook/create-react-app) (Quick local development setup)
 - [Official React tutorial](https://reactjs.org/tutorial/tutorial.html)
 - [Egghead.io React video tutorials](https://egghead.io/technologies/react)